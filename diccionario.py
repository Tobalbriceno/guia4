def menu(dic):
    print(  "Escoja una accion\n\n"
            "Agregar proteina[1]\n"
            "Editar proteina[2]\n"
            "Consultar proteina[3]\n"
            "Eliminar proteina[4]\n"
            "Imprimir diccionario [5]\n")

def editcode(dic):
    proteincode = str(input("Ingrese codigo de la proteina:\n"))
    if proteincode in dic:
        description = str(input("Ingrese descripcion de la proteina:\n"))
        dic[proteincode] = description
    else:
        print("La proteina ya existe")

def deletecode(dic):
    codein = str(input("Ingrese el codigo de la proteina:\n"))
    if codein in dic:
        del dic[codein]        
    else:
        print("No existe tal proteina")
 
def consultcode(dic):
    ext = str(input("Ingrese codigo de la proteina:\n"))
    if ext in dic:
        print(dic[ext])
    else:
        print("No existe tal proteina")        

def addcode(dic):
    nombre = str(input("ingrese nombre de la proteina:\n"))
    description = str(input("Ingrese descripcion de la proteina:\n"))
    if nombre in dic:
        print("ya existe esta proteina")
    else:
        dic.update({nombre: description})

def imprimirdic(dic):
    print(dic)
dic = { "7BQL": "The crystal structure of PdxI complex with the Alderene adduct",
        "7BQJ": "The structure of PdxI",
        "7BVZ": "Crystal structure of MreB5 of Spiroplasma citri bound to ADP",
        "6Q2C": "Domain swapped dimer of Acanthamoeba castellanii CYP51,
        "6PWN": "ADC-7 in complex with Beta-lactam antibiotic ceftazidime",
        "6PWL": "ADC-7 in complex with boronic acid transition state inhibitor LP06"}

resp = "s"
while resp != "N" and resp != "n":
    menu(dic)
    men = int(input("Que desea realizar\n"))
    if men == 2:
        editcode(dic)
    elif men == 1:
        addcode(dic)
    elif men == 3:
        consultcode(dic)
    elif men == 4:
        deletecode(dic)
    elif men == 5:
        imprimirdic(dic)
    if men == 1 or men == 2 or men == 3 or men == 4 or men == 5:
        resp = str(input("\ndesea salir? presione n para salir o cualquier letra para continuar:\n"))
    else:
        print("opcion invalida")